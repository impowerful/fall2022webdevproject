
 function DropDown({ options, onSelectionChange }) {
  return (
    <div className="selDiv">
    <label for="category">Choose Category:</label>
      <select name="category" id="categories" onChange={onSelectionChange}>
            {
                options.map(option => {
                    return (
                        <option>
                            {option}
                        </option>
                    )
                })
            }
      </select> 
    </div>
  )
}
//add a drop down categories and a drop down topic
export default DropDown;
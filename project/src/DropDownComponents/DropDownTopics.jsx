function DropDown({ options, onSelectionChange }) {
    return (
    <div className="selDiv">
        <label for="topic">Choose Topic:</label>
            <select name="topic" id="topics" onChange={onSelectionChange}>
                    {
                        options.map(option => {
                            return (
                                <option>
                                    {option}
                                </option>
                            )
                        })
                    }
            </select> 
    </div>
    )
  }
  //add a drop down categories and a drop down topic
  export default DropDown;
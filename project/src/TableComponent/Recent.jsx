function recentTopics(postList) {
    let recentTopic = [];

    postList.map( listing => {
        listing.topicList.forEach(topic => {
            topic.listPosts.forEach(list => {
                recentTopic.push(list);
            })
        })
    })

    recentTopic.sort((a,b) => a.date < b.date)
    return (
        <tbody>
            {
            recentTopic.map(elem => {
                return (
                    <tr>
                        <td>{elem.author}</td>
                        <td>{elem.date}</td>
                        <td>{elem.rate}</td>
                    </tr>
                )
            })
        }      
        </tbody>
    )
};

function Recent(props) {
    return (
        <>
        <table className="tableClass">
            <thead>
                <tr>
                    <th>author</th>
                    <th>date</th>
                    <th>rate</th>
                </tr>
            </thead>
            {recentTopics(props.postList)}
        </table> 
        </>
    );
}

export default Recent;
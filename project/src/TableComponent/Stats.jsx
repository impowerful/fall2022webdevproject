function statTopics(users) {
    let allTopics = users
    allTopics.sort((a,b) => a.nberPosts < b.nberPosts) //Sort doesn't work
return(
    <tbody>
    {
    allTopics.map(elem => {
        return (
            <tr>
                <td>{elem.user_id}</td>
                <td>{elem.nberPosts}</td>
            </tr>
        )
        
    })}
    </tbody>
    )
}

function Stats(props) {
  return (
      <>
      <table className="tableClass">
          <thead>
              <tr>
                  <th>author</th>
                  <th># of Post</th>
              </tr>
          </thead>
          {statTopics(props.users)}
      </table></>
  );
};

export default Stats;
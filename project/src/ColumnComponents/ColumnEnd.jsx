import Ranked from "../TableComponent/Ranked.jsx"
import Recent from "../TableComponent/Recent.jsx"
import Stats from "../TableComponent/Stats.jsx"
function ColumnEnd(props) {

  return (
    <fieldset id="endColumn">
        <div className="tables">
            <h3>Topic Status</h3>
              <div id="ranked"><Ranked topicList={props.object}/></div>
        </div>
        <div className="tables">
            <h3>Recent Posts</h3>
              <div id="recent"><Recent postList={props.object}/></div>
        </div>
        <div className="tables">
            <h3>User Stats</h3>
              <div id="stats"><Stats users={props.users}/></div>
              </div>
    </fieldset>
      
  )
}
export default ColumnEnd;

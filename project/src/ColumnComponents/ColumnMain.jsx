import{useState, useEffect} from "react"
import ListPosts from "../PostComponents/ListPosts";
import DropDownCategories from "../DropDownComponents/DropDownCategories";
import DropDownTopics from "../DropDownComponents/DropDownTopics";

const ColumnMain = function({ searchFilter, object, topicList, listPosts }) {

const [currentTopicList, setTopicList] = useState([]);
const [currentListPosts, setListPosts] = useState([]);
const [selectedCategory, setSelectedCategory] = useState();
const [filter, setFilter] = useState('')

const [categories, setCategories] = useState([
  "Category 1 - Coding - Computer Science",
  "Category 2 - Project Management",
  "Category 3 - Communication",
]);

  useEffect(() => {
    setSelectedCategory(categories[0]);
    const topics = object.filter((o) => {
      return o.name == categories[0];
    })[0]["topicList"];
    setTopicList(topics);

    const posts = topics.filter((topic) => {
      return topic == topics[0];
    })[0]["listPosts"];
    setListPosts(posts);
  }, []);

  // whenever the topic list is updated this function in useffect is called
  useEffect(() => {
    topicList(currentTopicList)
  }, [currentTopicList])

  // whenever the listPosts is updated this function in useffect is called
  useEffect(() => {
    listPosts(currentListPosts)
  }, [currentListPosts])

  // Handles category change
  const onCategoryChanged = (e) => {
    console.log(e.target.value);
    const category = e.target.value;
    setSelectedCategory(category);
    const topics = object.filter((o) => {
      return o.name == category;
    })[0]["topicList"];
    console.log(topics);
    setTopicList(topics);

    const posts = topics.filter((topic) => {
      return topic == topics[0];
    })[0]["listPosts"];
    setListPosts(posts);
  };

  // Handles topic change
  const onTopicChanged = (e) => {
    const topic = e.target.value;
    console.log(topic);
    const posts = currentTopicList.filter((t) => {
      return t.topic_title == topic;
    })[0]["listPosts"];
    setListPosts(posts);
  };

  // Filters the post based on search parameter

  const filteredListPost = currentListPosts.filter(lp => {
    return (
      lp.author.indexOf(searchFilter) != -1 ||
      lp.text.indexOf(searchFilter) != -1
    )
  })

return (
  <fieldset id="mainColumn"> 
      <div>
        <div id="selectors">
          <DropDownCategories
            options={categories}
            onSelectionChange={onCategoryChanged}
          />
          <DropDownTopics
            options={currentTopicList.map((t) => t.topic_title)}
            onSelectionChange={onTopicChanged}
          />
        </div>
      <div id="allPosts">
        {
        <ListPosts posts={filteredListPost}/>
        }
      </div>
    </div>
  </fieldset>
)
}

export default ColumnMain;


function ColumnEnd() {
    return (
      <fieldset id="startColumn">
          <div>
            <fieldset id="adminPanel">
                <legend align="center">Admin Panel</legend>
                    <button id="createCat">Create Category</button>
                    <button id="createTopic">Create Topic</button>
                    <button id="closeTopic">Close Topic</button>
                    <button id="delTopic">Delete Topic</button>
            </fieldset>
          </div>
      </fieldset>
    )
  }
  export default ColumnEnd;

const Header = ({ onSearch }) => {

    return (
        <header>
            <h1>Pseudo Forum</h1>
            <input onChange={(e) => onSearch(e.target.value)} type={'search'} name='search' id='search' placeholder="Search" />
        </header>)
        }
export default Header;


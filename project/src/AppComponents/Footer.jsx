//Information to display the footer
const Footer = function() {
    return (
        <footer>
            <h3> Project 2- Using React </h3>
                <br/>
                <p> Yu Hua Yang 2133677</p>
                <br/>
                <p> Amatta Siripraphanh 2039170</p>
                <br/>
            <h3>Copyright &copy; Fall 2022</h3>
        </footer>
    )
}
export default Footer;